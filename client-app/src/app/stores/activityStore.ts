import { observable, action, computed, runInAction } from "mobx";
import { SyntheticEvent } from "react";
import { IActivity } from "../models/activity";
import agent from "../api/agent";
import { history } from "../..";
import { toast } from "react-toastify";
import { RootStore } from "./rootStore";
import { setActivityProps, createAtendee } from "../common/util/util";

export default class ActivityStore {
  rootStore: RootStore;
  constructor(Rootstore: RootStore) {
    this.rootStore = Rootstore;
  }

  @observable activityRegistry = new Map();
  @observable activity: IActivity | null | undefined;
  @observable loadingInitial = false;
  @observable submiting = false;
  @observable target = "";
  @observable loading=false;

  @computed get activitiesByDate() {
    return this.groupActivitiesByDate(
      Array.from(this.activityRegistry.values())
    );
  }
  groupActivitiesByDate(activities: IActivity[]) {
    const sortedActivities = activities.sort(
      (a, b) => a.date.getTime() - b.date.getTime()
    );
    return Object.entries(
      sortedActivities.reduce((activities, activity) => {
        const date = activity.date.toISOString().split("T")[0];
        activities[date] = activities[date]
          ? [...activities[date], activity]
          : [activity];
        return activities;
      }, {} as { [key: string]: IActivity[] })
    );
  }

  @action loadActivities = async () => {
    this.loadingInitial = true;

    try {
      const activities = await agent.Activities.list();
      runInAction("loading activities", () => {
        activities.forEach(activity => {
          setActivityProps(activity, this.rootStore.userStore.user!);
          this.activityRegistry.set(activity.id, activity);
        });
        this.loadingInitial = false;
      });
    } catch (error) {
      runInAction("load activities error", () => {
        console.log(error);
        this.loadingInitial = false;
      });
    }
  };

  @action loadActivity = async (id: string) => {
    let activity = this.getActivity(id);
    if (activity) {
      this.activity = activity;
      return activity;
    } else {
      this.loadingInitial = true;
      try {
        activity = await agent.Activities.details(id);
        runInAction("getting activity", () => {
          setActivityProps(activity, this.rootStore.userStore.user!);
          this.activity = activity;
          this.activityRegistry.set(activity.id, activity);
          this.loadingInitial = false;
        });
        return activity;
      } catch (error) {
        runInAction("get activity error", () => {
          this.loadingInitial = false;
        });
        //throw error;
      }
    }
  };
  @action clearActivity = () => {
    this.activity = null;
  };
  getActivity = (id: string) => {
    return this.activityRegistry.get(id);
  };
  @action createActivity = async (activity: IActivity) => {
    this.submiting = true;
    try {
      await agent.Activities.create(activity);
      const attende = createAtendee(this.rootStore.userStore.user!);
      attende.isHost=true;
      let attendees=[];
      attendees.push(attende);
      activity.attendees=attendees;
      activity.isHost=true;
      runInAction("creating activity", () => {        
        this.activityRegistry.set(activity.id, activity);
        this.submiting = false;
      });
      history.push(`/activities/${activity.id}`);
    } catch (error) {
      runInAction("create activity error", () => {
        this.submiting = false;
      });
      toast.error("Problem submitting data");
      console.log(error.response);
    }
  };
  @action editActivity = async (activity: IActivity) => {
    this.submiting = true;
    try {
      await agent.Activities.update(activity);
      runInAction("edit activity", () => {
        this.activityRegistry.set(activity.id, activity);
        this.activity = activity;
        this.submiting = false;
      });
      history.push(`/activities/${activity.id}`);
    } catch (error) {
      runInAction("edit activity error", () => {
        this.submiting = false;
      });
      toast.error("Problem submitting data");
      console.log(error.response);
    }
  };
  @action deleteActivity = async (
    event: SyntheticEvent<HTMLButtonElement>,
    id: string
  ) => {
    this.submiting = true;
    this.target = event.currentTarget.name;
    try {
      await agent.Activities.delete(id);
      runInAction("deliting activity", () => {
        this.activityRegistry.delete(id);
        this.submiting = false;
        this.target = "";
      });
    } catch (error) {
      runInAction("delite activity error", () => {
        this.submiting = false;
        this.target = "";
        console.log(error);
      });
    }
  };
  @action attendActivity =  async () => {
    const attendee = createAtendee(this.rootStore.userStore.user!);
    this.loading=true;
    try{
      await agent.Activities.attend(this.activity!.id);
      runInAction(()=>{
        if (this.activity) {
          this.activity.attendees.push(attendee);
          this.activity.isGoing = true;
          this.activityRegistry.set(this.activity.id, this.activity);
          this.loading=false;
        }

      })
    }
    catch(error){
      runInAction(()=>{
        this.loading=false;
      })  
      toast.error('Problem signing up to activity');
    }

  };
  @action cancleAttendance = async () => {
    this.loading=true;
    try{
      await agent.Activities.unattend(this.activity!.id);
      runInAction(()=>{
        if (this.activity) {
          this.activity.attendees = this.activity.attendees.filter(
            a => a.username !== this.rootStore.userStore.user!.username
          );
          
          this.activity.isGoing = false;
          this.activityRegistry.set(this.activity.id,this.activity);
          this.loading=false;
        }

      })
    }
    catch(error){
      runInAction(()=>{
        this.loading=false;
      })
      toast.error('Problem canceling attendance')
    }

  };
}
