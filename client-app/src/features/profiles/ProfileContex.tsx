import React from 'react'
import {Tab} from 'semantic-ui-react';
import { ProfilePhotos } from './ProfilePhotos';


const panes = [
    {menuItem : 'About', render: () => <Tab.Pane>About context</Tab.Pane>},
    {menuItem : 'Photos', render: () => <ProfilePhotos />},
    {menuItem : 'Activities', render: () => <Tab.Pane>Activities context</Tab.Pane>},
    {menuItem : 'Followers', render: () => <Tab.Pane>Followers context</Tab.Pane>},
    {menuItem : 'Following', render: () => <Tab.Pane>Following context</Tab.Pane>}
]

export const ProfileContex = () => {
    return (
        <Tab 
            menu={{fluid: true, vertical:true}}
            menuPosition='right'
            panes={panes}
        />
    )
}
